---------------------------------------------------------------------------
-- Copyright (C) 2019 KhronoSync Digital Solutions LLC
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>
---------------------------------------------------------------------------

-- Add distro Peppermint OS.
INSERT INTO distros (id, name, description, description_source, homepage, tags, logo_image_h, logo_image_v, logo_bg_color)
VALUES
(
    '62a9e05f-439d-4911-9ce2-0714a77ed808',
    'PeppermintOS',
    'A lightning fast, lightweight Linux based OS, that is secure, stable and fully ' ||
    'customizable. Peppermint has long term support with minimal application choices made for ' ||
    'you. We provide the tools to make the system your own, you are in charge!',
    'https://peppermintos.com/',
    'https://peppermintos.com/',
    'Workstation,Server,IoT,Cloud',
    'peppermint-logo-2016-h.svg',
    'peppermint-logo-2016-v.svg',
    '#ffffff'
);