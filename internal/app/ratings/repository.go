/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package ratings

import (
  "github.com/go-pg/pg"
  uuid "github.com/satori/go.uuid"
  "log"
)

type RatingRepository struct {
  db *pg.DB
}

func (rr *RatingRepository) Create(rm *RatingModel) (*RatingModel, error) {
  _, err := rr.db.Model(rm).Returning("*").Insert()
  if err != nil {
    log.Printf("Error while creating new Rating. Reason: %v\n", err)
    return nil, err
  }

  log.Printf("Created new Rating '%v'\n", rm.Id)
  return rm, nil
}

func (rr *RatingRepository) Read() ([]RatingModel, error) {
  var ratings []RatingModel
  err := rr.db.Model(&ratings).Select()
  if err != nil {
    log.Printf("Error reading all Ratings. Reason: %v\n", err)
    return nil, err
  }

  log.Printf("Successfully retrieved ratings\n")
  return ratings, nil
}

func (rr *RatingRepository) ReadByDistroId(
  distroId uuid.UUID,
  sortOrder string,
) ([]RatingModel, error) {
  var ratings []RatingModel
  err := rr.db.
      Model(&ratings).
      Where("distro_id = ?", distroId).
      Order(sortOrder).
      Select()

  if err != nil {
    log.Printf(
      "Error reading Ratings for Distro %v. Reason: %v\n",
      distroId, err,
    )
    return nil, err
  }

  return ratings, nil
}

func (rr *RatingRepository) ReadById(id uuid.UUID) (*RatingModel, error) {
  rating := &RatingModel{Id: id}
  err := rr.db.Select(rating)
  if err != nil {
    log.Printf("Error reading Rating with ID=%v. Reason: %v\n", id, err)
    return nil, err
  }

  return rating, nil
}

func(rr *RatingRepository) DeleteById(id uuid.UUID) (*RatingModel, error) {
  rating := &RatingModel{Id: id}
  err := rr.db.Delete(rating)
  if err != nil {
    log.Printf("Error deleting Rating with ID=%v. Reason: %v\n", id, err)
    return nil, err
  }

  return rating, nil
}

func (rr *RatingRepository) MarkUseful(id uuid.UUID) (*RatingModel, error) {
  rating, err := rr.ReadById(id)
  if err != nil {
    return nil, err
  }

  rating.Upvotes += 1

  err = rr.db.Update(rating)
  if err != nil {
    log.Printf("Error updating Rating with ID=%v. Reason: %v\n", id, err)
    return nil, err
  }

  return rating, nil
}